<?php
// Author › Heavenly Waltz ( HW )
// Project › Name: HW Saver Image
// Project module › ENG: Convert any images to the specified graphic format.
// Project module › RUS: Конвертация любых изображений в указанный графический формат.
// version › 1.0

class HW_saverImage{
	private $_source_path_img = NULL; // Изначальный путь картинки
	private $_source_name_img = NULL; // Изначальное имя картинки $file = basename($url);
	private $_source_info_img = NULL; // Содержит информацию о загруженной картинки
	private $_max_width = 1024; // максимальное значение картинки по ширине.
	private $_max_height = 768; // максимальное значение картинки по высоте.	
	private $_min_width =  10; // минимальные значение картинки по ширине.
	private $_min_height = 10; // минимальные значение картинки по высоте.
	private $_min_kb = 10; // минимальные вес картинки в КилоБайтах
	private $_type_save_img = 'webp'; // webp , jpeg , gif , png. формат в котором сохронять изображения.
	private $_dir_saved = 'HW_Saver_Img'; // папка в которую сохранять изображения.
	// "save" - пропорции картинки сохранятся при её сжатии  до _max_ размеров.
	// "max" - картинка растянится до _max_ размеров принудительно не зависемо от своих размерв.
	private $_type_proportions = "save"; 
	// Установит мета данные на картинку
	private $_meta_data = NULL;  // ПОКА НЕРАБОТАЕТ!!! будет устанавливать мета данные в картинку
	
	// НЕ МЕНЯТЬ БЕЗ УЧАСТИЯ РАЗРАБОТЧИКА !!!!!!!
	private $_supported_formats_img = ['webp','jpeg','gif','png']; // форматы картинок что поддерживает HW_saverImage 
	
	function __construct(){} // Цель сделать без конструктора ( так заказали )
	
	// -- Сеттеры. Устанавливают всякие параметры для работы объекта --
	// Установит папку для сохранения
	public function setSaveDir($path_dir = false){ if ($path_dir) {
		$this->_dir_saved = $path_dir;
	} else {
		return $this->retError('NO path(url) dir');
	}}
	
	// Установим максимальные размеры для картинки, оба параметра необязательные
	public function setMaxSize($width = false,$height = false){
		if($width){  $this->_max_width  = intval($width); }
		if($height){ $this->_max_height = intval($height);}
	}
	
	// Установим минимальные размеры для картинки, оба параметра необязательные
	public function setMinSize($width = false,$height = false){
		if($width){  $this->_min_width  = intval($width); }
		if($height){ $this->_min_height = intval($height);}
	}
	
	// Установим минимальный вес для картинки
	public function setMinKb($kb = false){
		if($kb){  $this->_min_kb  = intval($kb); }
	}
	
	// Вкл\Откл режима сохранения пропорций. если стоит = 100 на 50, а картинка прилитела 321 на 321, то она сохраниться как 50 на 50, так как её пропорции 1 к 1, и сохранение произайдёт по на именьшему параметру.
	public function setTupeProportions($val = "save"){
		$this->_type_proportions = $val;
	}
	
	// установит тип для сохроняемых картинок
	public function setTypeSaveImg($type_img = false){
		if($type_img){  $this->_type_save_img  = trim(strip_tags($type_img)); }
	}
	
	// Скажем нашему классу путь до картинки
	public function setPathImg($path_img = false){
		if($path_img){
			$path_img = trim(strip_tags($path_img));
			$this->_source_path_img = $path_img;
			$this->_source_name_img = pathinfo(basename($this->_source_path_img))['filename'];
			return $this->retOk('Path imge set.');
		} else {
			return $this->retError('NO path(url) img');
		}
	}
	
	// Установим картинку в объект
	public function setImg($path_img = false){
		$path = $this->controlPathImg($path_img);
		
		if($path['res'] == 'OK'){
			$imgInfo = $this->createImgInfo();
			if($imgInfo['res'] == 'OK'){
				$this->_source_info_img = $imgInfo['info'];
				return $this->retOk('Info imge set.');
			} else {
				return $imgInfo;
			}
		}
		else {
			return $this->retError('NO path(url) img');
		}
	}

	// Функция сохранения картинки, принимает  $path_img - пут до картинки.
	public function saveImg($path_img = false){
		$path = $this->controlPathImg($path_img);
		
		if($path['res'] == 'OK'){
			$this->testSaveDir(); // проверим папку, если нету создадим
			$imgInfo = $this->setImg(); // загрузим информацию о текущей картинке
			if($imgInfo['res'] == 'OK'){ // если все хорошо, и на стадии сбора нужной информации не произошло ошибки.
				$GDtarget = $this->createGD(); // создадим GD объект из полученной картинки
				if($GDtarget['res'] == "OK"){ // если нам удалось создать из картинки представление GD
					$saveImg = $this->saveGDImg($GDtarget['info']);// сохраним представления GD в указанную папку, с указанным форматом.
					if($saveImg['res'] == 'OK') {
						return $this->retOk(['img_name' => $saveImg['info'],'info'=>'img saved']);
					}
					else {return $this->retError('img not saved!');}
				} else {
					return $GDtarget;
				}
			}else {
				return $imgInfo;
			}
		} else {
			return $this->retError('NO path(url) img');
		}
	}
	
	// Просто удалит картинку
	public function deleteImg($path_img = false){
		$path = $this->controlPathImg($path_img);
		
		if($path['res'] == 'OK'){
			$imgInfo = $this->setImg(); // загрузим информацию о текущей картинке
			if($imgInfo['res'] == 'OK'){
				if($this->_source_info_img['from'] == "L"){
					if($this->_source_path_img  && file_exists($this->_source_path_img)){
						unlink($this->_source_path_img);
						return $this->retOk('delete');
					}
					else {return $this->retError('NO file img');}
				} else {return $this->retError('NO Local img');}
			} else { return $imgInfo; }
		} else { return $this->retError('NO path(url) img'); }
	}
	
	// Функция Удалит картинку если её размеры меньше или равны минимальным
	public function deleteImgMinWH($path_img = false){
		$path = $this->controlPathImg($path_img);
		
		if($path['res'] == 'OK'){
			$imgInfo = $this->setImg(); // загрузим информацию о текущей картинке
			if($imgInfo['res'] == 'OK'){
				if($this->_source_info_img['from'] == "L"){
					$sizeImg = getimagesize($this->_source_path_img);
					$img_w = $sizeImg[0];
					$img_h = $sizeImg[1];
					
					if($img_w <= $this->_min_width && $img_h <= $this->_min_height){
						$this->_source_name_img = NULL;
						$this->_source_info_img = NULL;
						if($this->_source_path_img  && file_exists($this->_source_path_img)){
							unlink($this->_source_path_img);
							$this->_source_path_img = NULL;
							return $this->retOk('delete min size');
						}
						else {$this->_source_path_img = NULL; return $this->retError('NO file img');}
						//return $this->retOk('delete min size');
					}
					return $this->retOk('no delete min size');
				} else {return $this->retError('NO Local img');}
			} else { return $imgInfo; }
		} else { return $this->retError('NO path(url) img'); }
	}
	// Функция Удалит картинку если её вес в kb меньше или равны минимальным
	public function deleteImgMinKB($path_img = false){
		$path = $this->controlPathImg($path_img);
		
		if($path['res'] == 'OK'){
			$imgInfo = $this->setImg(); // загрузим информацию о текущей картинке
			if($imgInfo['res'] == 'OK'){
				if($this->_source_info_img['from'] == "L"){
					if($this->_source_info_img['size_kb'] <= $this->_min_kb){
						$this->_source_name_img = NULL;
						$this->_source_info_img = NULL;
						if($this->_source_path_img  && file_exists($this->_source_path_img)){
							unlink($this->_source_path_img);
							$this->_source_path_img = NULL;
							return $this->retOk('delete min kb');
						}
						else {$this->_source_path_img = NULL; return $this->retError('NO file img');}
					}
					return $this->retOk('no delete min kb');
				} else {return $this->retError('NO Local img');}
			} else { return $imgInfo; }
		} else { return $this->retError('NO path(url) img'); }
	}
	


	// -- вспомогательные функции --

	// Просто оглашатель ошибок
	private function retError($text){
		return [
			'res' => 'ERROR', 
			'info' => "HW_saverImage:ERROR - $text",
		];
	}
	// Просто оглашатель успешности
	private function retOk($info){
		return [
			'res' => 'OK', 
			'info' => $info,
		];
	}

	// Проверит наличие папки, и если нужно создаст её.
	private function testSaveDir() { if(!is_dir($this->_dir_saved)){ mkdir($this->_dir_saved,0755,true); } }
	// Корректно проверяет добавление нового пути до картинки
	private function controlPathImg($path_img) {
		$path = [];
		if($path_img) {$path = $this->setPathImg($path_img);}
		else if($this->_source_path_img) {$path = $this->retOk('_source_path_img true');}
		else {$path = $this->retError('_source_path_img false');}
		return $path;
	}
	
	// Создас спец информацию по картинке для дальнейшер работы
	private function createImgInfo() {
		$imgInformer = [
			'from'=>'L', // откуда картинки L - локальная G - глобальная ( из ссылки )
			'type'=>'', // тип картинки. webp , jpeg , gif , png
		];
		//$pos3 = preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',$file);
		$pos = strpos($this->_source_path_img,'http://');
		$pos2 = strpos($this->_source_path_img,'https://'); 
		
		if ($pos !== false || $pos2 !== false) {
			$imgInformer['from'] = 'G';
			$imgHead = get_headers($this->_source_path_img);
			if(strpos($imgHead[0],'200')){ // если мы смогли получить картинку от сервера
				//Может пригодится в будущем что-либо тут записать :)
			}else {
				return $this->retError("Server not 200! say: {$imgHead[0]}");
			}
		} else if(file_exists(str_replace('//','/',$this->_source_path_img))){ // если файл локальный и существует.
			$imgInformer['size_kb'] = intval(filesize($this->_source_path_img) / 1024);
		} else {
			return $this->retError("Image not found");
		}
		
		$imgInformer['type'] = pathinfo($this->_source_path_img,PATHINFO_EXTENSION );
		$imgInformer['type'] = trim(strip_tags($imgInformer['type']));
		switch($imgInformer['type']){ // немного корректности в нашем мире :)
			case('jpg'): $imgInformer['type'] = "jpeg"; break; 
			case('jpe'): $imgInformer['type'] = "jpeg"; break; 
		}
		// Проверим поддерживаем ли мы данный формат
		if(in_array($imgInformer['type'],$this->_supported_formats_img)){
			//$imgInformer['size'] = getimagesize($this->_source_path_img);
		} else {
			return $this->retError("Unsupported format");
		}

		return $this->retOk($imgInformer);;
	}
	
	// Создас из картинки GD объект и вернёт его
	private function createGD() {
		if($this->_source_info_img['type']){
			$GDtarget = NULL;
			switch($this->_source_info_img['type']){ 
				case('webp'): $GDtarget = @imagecreatefromwebp($this->_source_path_img); break; 
				case('jpeg'): $GDtarget = @imagecreatefromjpeg($this->_source_path_img); break; 
				case('gif'):  $GDtarget = @imagecreatefromgif($this->_source_path_img); break; 
				case('png'):  $GDtarget = @imagecreatefrompng ($this->_source_path_img); break; 
			}
			
			if(!$GDtarget){
				return $this->retError("GD create error"); 
			} else {
				return $this->retOk($GDtarget);
			}
		}
		return $this->retError("_source_info_img = NULL");
	}
	
	
	// Сохраним GD картинку,
	private function saveGDImg($GD) {
		$GDdata = [];
		$GDdata['GD'] = $GD;
		$GDdata['width'] = imagesx($GD);
		$GDdata['height'] = imagesy($GD);
		$GDdata['new_width'] = $GDdata['width'];
		$GDdata['new_height'] = $GDdata['height'];
		$GDdata['new_url'] = $this->checkName();
		// проверим нужны ли нам пропорции
		if($this->_type_proportions == "save"){
			$ratio = $GDdata['width'] / $GDdata['height']; // intval();
			// Если размер изображения больше максимального
			if($GDdata['width'] > $this->_max_width || $GDdata['height'] > $this->_max_height){
				$new_w = $this->_max_width;
				$new_h = $this->_max_width / $ratio;
				if($new_h > $this->_max_height){
					$new_w = $this->_max_height * $ratio;
					$new_h = $this->_max_height;
				}
				$GDdata['new_width'] = intval($new_w);
				$GDdata['new_height'] = intval($new_h);
			}
		} else if( $this->_type_proportions == "max"){ //если пропорции не нужны
			$GDdata['new_width'] = $this->_max_width;
			$GDdata['new_height'] = $this->_max_height;
		}

		switch($this->_type_save_img){
			case('webp'): $this->saveGDWebp($GDdata); break; 
			case('jpeg'): $this->saveGDJpeg($GDdata); break; 
			case('gif'):  $this->saveGDGif ($GDdata); break; 
			case('png'):  $this->saveGDPng ($GDdata); break; 
		}
		
		return $this->retOk($GDdata['new_url']);
	}
	// Проверит существование файла с тамким именем, вернёт имя с которым можно сохранить
	private function checkName() {
		$name = str_replace('//','/',$this->_dir_saved.'/'.$this->_source_name_img);
		$iter_name = 1;
		while(file_exists($name.'.'.$this->_type_save_img)){
			if($iter_name <= 9){$dopN = '-0'.$iter_name;}
			else {$dopN = '-'.$iter_name;}
			$name = str_replace('//','/',$this->_dir_saved.'/'.$this->_source_name_img.$dopN);
			$iter_name++;
		}

		return $name;
	}
	// сохранит картинку как webp
	private function saveGDWebp($GDdata) {
		$output = imagecreatetruecolor($GDdata['new_width'], $GDdata['new_height']);
		imagealphablending($output, false);
		imagesavealpha($output, true);
		
		imagecopyresampled($output, $GDdata['GD'], 0, 0, 0, 0, $GDdata['new_width'], $GDdata['new_height'],$GDdata['width'], $GDdata['height']);
		
		imagewebp($output,$GDdata['new_url'].'.'.$this->_type_save_img);
		imagedestroy($output);
		imagedestroy($GDdata['GD']);
	}
	// сохранит картинку как png
	private function saveGDPng($GDdata) {
		$output = imagecreatetruecolor($GDdata['new_width'], $GDdata['new_height']);
		imagealphablending($output, false);
		imagesavealpha($output, true);
		
		imagecopyresampled($output, $GDdata['GD'], 0, 0, 0, 0, $GDdata['new_width'], $GDdata['new_height'],$GDdata['width'], $GDdata['height']);
		
		imagewebp($output,$GDdata['new_url'].'.'.$this->_type_save_img);
		imagedestroy($output);
		imagedestroy($GDdata['GD']);
	}	
	// сохранит картинку как jpeg
	private function saveGDJpeg($GDdata) {
		$path = $GDdata['new_url'].'.'.$this->_type_save_img;
		$output = imagecreatetruecolor($GDdata['new_width'], $GDdata['new_height']);
		$white = imagecolorallocate($output,  255, 255, 255);
		imagefill($output,0,0,$white);
		
		imagecopyresampled($output, $GDdata['GD'], 0, 0, 0, 0, $GDdata['new_width'], $GDdata['new_height'],$GDdata['width'], $GDdata['height']);
		
		imagewebp($output,$path);
		imagedestroy($output);
		imagedestroy($GDdata['GD']);
	}
	// сохранит картинку как gif
	private function saveGDGif($GDdata) {
		$output = imagecreatetruecolor($GDdata['new_width'], $GDdata['new_height']);
		$white = imagecolorallocate($output,  255, 255, 255);
		imagefill($output,0,0,$white);
		
		imagecopyresampled($output, $GDdata['GD'], 0, 0, 0, 0, $GDdata['new_width'], $GDdata['new_height'],$GDdata['width'], $GDdata['height']);
		
		imagewebp($output,$GDdata['new_url'].'.'.$this->_type_save_img);
		imagedestroy($output);
		imagedestroy($GDdata['GD']);
	}
}

// $HWSaver = new HW_saverImage();
